## To Use


```bash
# Clone this repository
git clone https://gitlab.com/riad.saboundji/neurowalletapp.git
# Go into the repository
cd neurowalletapp
# Install dependencies
npm install
# Run the app
npm start
```


## Wallet Doc

# 1- Project NeuroWallet

  - run Docker compose
    * open http://localhost:8081/
  - Build project with : yarn build
  - To change urls for begin connexion : src/utils/backapi.js
    * urlBots Arrays 
  - App used LocalStorage (key-value)
  - Key crypting : aes 256 in file : src/utils/keys.js


# 2 - Project Desktop-APP

this project used electron-quick-start project, juste run npm install 

  - build NeuroWallet with option mobile as false in file :
    * src/App.js:13 , global.mobileversion = false
  - Copy all file in Build dir from in root directory
  - to build exec file : 
     * electron-packager . --overwrite --platform=win32 --arch=x64 --prune=true --out=release-builds 
  - or build for all : 
     * electron-packager . --all --prune=true --out=release-builds 
  - for information for options of build see : https://github.com/electron-userland/electron-packager

  - TODO :
    * Fix menu in linux version
    * use real db from electron version for best perf
    * add support smart contact coding

# 3 - Project Mobile

this project used cordova lib pour build android/ios application,

  - build NeuroWallet with option mobile a true in file :
    * src/App.js:13 , global.mobileversion = true
  - Copy all file in Build dir from (1) in src/www directory
  - run Docker compose en cli mode
  - build android app:
    * cordova build android

  - TODO : convert React code to ReactNative code for best performence ( don't used cordova web-app )